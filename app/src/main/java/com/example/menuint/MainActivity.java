package com.example.menuint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.GridView;
import android.view.View;
import android.widget.Toast;

import com.example.menuint.Interfaces.PlatilloApi;
import com.example.menuint.Modelos.Platillo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private GridView menu;
    private AdaptadorPlatillos adaptador;
    private ArrayList<Platillo> platillos = new ArrayList<>();
    private PlatilloApi platilloApi;

    public MainActivity() {
        platilloApi = new API().platilloApi();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adaptador = new AdaptadorPlatillos(this, platillos);
        menu = (GridView) findViewById(R.id.menu);
        menu.setAdapter(adaptador);
        obtenerPlatillos();
        escucharSeleccion();
    }

    private void obtenerPlatillos() {
        Call<ArrayList<Platillo>> call = platilloApi.getPlatillos();
        call.enqueue(new Callback<ArrayList<Platillo>>() {
            @Override
            public void onResponse(Call<ArrayList<Platillo>> call, Response<ArrayList<Platillo>> response) {
                try {
                    if (response.isSuccessful()) {
                        for (Platillo p: response.body()) {
                            platillos.add(p);
                        }
                        adaptador.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Platillo>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void escucharSeleccion(){
        menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int elementId, long l) {
                switch (elementId){
                    case 0:
                        Intent intent = new Intent(MainActivity.this, FormatoPlatillosLista.class);
                        startActivity(intent);
                        break;
                    default:
                        Toast.makeText(MainActivity.this,"No hay información de ese platillo", Toast.LENGTH_SHORT).show();
                }
            }
        }) ;
    }
}