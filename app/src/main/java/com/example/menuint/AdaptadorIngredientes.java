package com.example.menuint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.menuint.Modelos.ModeloIngrediente;
import com.example.menuint.Modelos.Platillo;
import java.util.ArrayList;

public class AdaptadorIngredientes extends BaseAdapter {
    private ArrayList<ModeloIngrediente> ingredientes;
    private Context context;
    private API Api = new API();

    public AdaptadorIngredientes(Context context, ArrayList<ModeloIngrediente> ingredientes){
        this.context = context;
        this.ingredientes = ingredientes;
    }

    @Override
    public int getCount() {
        return ingredientes.size();
    }

    @Override
    public Object getItem(int i) {
        return ingredientes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ModeloIngrediente ingrediente = (ModeloIngrediente) getItem(i);
        view = LayoutInflater.from(context).inflate(R.layout.activity_ingrediente,null);
        CheckBox checkBox = view.findViewById(R.id.checkBoxIngredientes);
        checkBox.setText(ingrediente.getNombre());

        return view;
    }
}