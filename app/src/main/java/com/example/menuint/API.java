package com.example.menuint;
import com.example.menuint.Interfaces.PlatilloApi;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    String baseRoute = "http://192.168.1.104:5013/";
    private Retrofit api = new Retrofit.Builder()
            .baseUrl(baseRoute)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public String getBaseRoute() {
        return baseRoute;
    }

    public PlatilloApi platilloApi() {
        return api.create(PlatilloApi.class);
    }
}
