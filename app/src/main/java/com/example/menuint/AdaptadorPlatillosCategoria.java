package com.example.menuint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.menuint.Modelos.CategoriaPlatillo;

import java.util.ArrayList;


    public class AdaptadorPlatillosCategoria extends BaseAdapter {
        private ArrayList<CategoriaPlatillo> platillos;
        private Context context;
        private API Api = new API();

        public AdaptadorPlatillosCategoria(Context context, ArrayList<CategoriaPlatillo> platillos){
            this.context = context;
            this.platillos = platillos;
        }

        @Override
        public int getCount() {
            return platillos.size();
        }

        @Override
        public Object getItem(int i) {
            return platillos.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            CategoriaPlatillo platillo = (CategoriaPlatillo)  getItem(i);
            view = LayoutInflater.from(context).inflate(R.layout.activity_formato_platillos_lista,null);

            ImageView imagen = view.findViewById(R.id.Imagen);

            TextView nombre = view.findViewById(R.id.Nombre);
            nombre.setText(platillo.getNombre());

            Glide.with(view.getContext()).load(Api.getBaseRoute() + "imagenes/" + platillo.getImagen()).into(imagen);

            return view;
        }
    }

