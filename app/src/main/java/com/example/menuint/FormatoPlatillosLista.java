package com.example.menuint;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.menuint.Interfaces.PlatilloApi;
import com.example.menuint.Modelos.CategoriaPlatillo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormatoPlatillosLista extends AppCompatActivity {

    AdaptadorPlatillosCategoria adaptador;
    GridView menu;
    ArrayList<CategoriaPlatillo> platillos = new ArrayList<>();
    PlatilloApi platilloApi;

    public FormatoPlatillosLista() {
        platilloApi = new API().platilloApi();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formato_platillos_categoria);
        adaptador = new AdaptadorPlatillosCategoria(this, platillos);
        menu = (GridView) findViewById(R.id.menu);
        menu.setAdapter(adaptador);
        obtenerPlatillos();
        escucharSeleccion();
    }

    private void obtenerPlatillos() {
        Call<ArrayList<CategoriaPlatillo>> call = platilloApi.getCategoriaPlatillos();
        call.enqueue(new Callback<ArrayList<CategoriaPlatillo>>() {
            @Override
            public void onResponse(Call<ArrayList<CategoriaPlatillo>> call, Response<ArrayList<CategoriaPlatillo>> response) {
                try {
                    if (response.isSuccessful()) {
                        for (CategoriaPlatillo p : response.body()) {
                            platillos.add(p);
                        }
                        adaptador.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CategoriaPlatillo>> call, Throwable t) {
                Toast.makeText(FormatoPlatillosLista.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void escucharSeleccion() {
        menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int elementId, long l) {
                switch (elementId) {
                    case 0:
                        Intent intent = new Intent(FormatoPlatillosLista.this, detallesPlatillo.class);
                        intent.putExtra("imagenPlatillo",platillos.get(elementId).getImagen());
                        int idp = platillos.get(elementId).getId();
                        intent.putExtra("idPlatillo",idp);
                        startActivity(intent);
                        break;
                    default:
                        Toast.makeText(FormatoPlatillosLista.this, "No hay información de ese platillo", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}