package com.example.menuint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.menuint.Modelos.Platillo;
import java.util.ArrayList;

public class AdaptadorPlatillos extends BaseAdapter {
    private ArrayList<Platillo> platillos;
    private Context context;
    private API Api = new API();

    public AdaptadorPlatillos(Context context, ArrayList<Platillo> platillos){
        this.context = context;
        this.platillos = platillos;
    }

    @Override
    public int getCount() {
        return platillos.size();
    }

    @Override
    public Object getItem(int i) {
        return platillos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Platillo platillo = (Platillo) getItem(i);
        view = LayoutInflater.from(context).inflate(R.layout.activity_formato_platillos,null);

        ImageView imagen = view.findViewById(R.id.Imagen);

        TextView nombre = view.findViewById(R.id.Nombre);
        nombre.setText(platillo.getNombre());

        Glide.with(view.getContext()).load(Api.getBaseRoute() + "imagenes/" + platillo.getImagen()).into(imagen);

        return view;
    }
}