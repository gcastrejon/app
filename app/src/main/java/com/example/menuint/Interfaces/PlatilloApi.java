package com.example.menuint.Interfaces;

import com.example.menuint.Modelos.CategoriaPlatillo;
import com.example.menuint.Modelos.ModeloIngrediente;
import com.example.menuint.Modelos.Platillo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PlatilloApi {
    @GET("Platillos")
    Call<ArrayList<Platillo>> getPlatillos();
    @GET("CategoriaPlatillos")
    Call<ArrayList<CategoriaPlatillo>> getCategoriaPlatillos();
    @GET("Ingredientes")
    Call<ArrayList<ModeloIngrediente>> getIngredientes(@Query("id") int id);
}
