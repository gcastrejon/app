package com.example.menuint;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.menuint.Interfaces.PlatilloApi;
import com.example.menuint.Modelos.ModeloIngrediente;
import com.example.menuint.Modelos.Platillo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class detallesPlatillo extends AppCompatActivity {
    private ArrayList <ModeloIngrediente> ingredientes = new ArrayList<>();
    private ListView listaIngredientes;
    private AdaptadorIngredientes adaptador;
    private PlatilloApi platilloApi;
    private API Api = new API();
    public detallesPlatillo() {
        platilloApi = new API().platilloApi();
    }
    int idPlatillo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalles_platillo);

        String imagen = getIntent().getExtras().getString("imagenPlatillo");
        idPlatillo = getIntent().getExtras().getInt("idPlatillo");


        adaptador = new AdaptadorIngredientes(this, ingredientes);
        listaIngredientes = (ListView) findViewById(R.id.listaIngredientes);
        listaIngredientes.setAdapter(adaptador);
        ImageView img = (ImageView) findViewById(R.id.imagen);
        Glide.with(detallesPlatillo.this).load(Api.getBaseRoute() + "imagenes/" + imagen).into(img);
        obtenerIngredientes();

    }
    public void regresarPantalla(View view){
        finish();
    }

    public void obtenerIngredientes(){
        Call<ArrayList<ModeloIngrediente>> call = platilloApi.getIngredientes(idPlatillo);
        call.enqueue(new Callback<ArrayList<ModeloIngrediente>>() {
            @Override
            public void onResponse(Call<ArrayList<ModeloIngrediente>> call, Response<ArrayList<ModeloIngrediente>> response) {
                try {
                    if (response.isSuccessful()) {
                        for (ModeloIngrediente p: response.body()) {
                            ingredientes.add(p);
                        }
                        adaptador.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    Log.d("ERROR", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModeloIngrediente>> call, Throwable t) {
                Toast.makeText(detallesPlatillo.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}