package com.example.menuint.Modelos;

import com.google.gson.annotations.SerializedName;

public class CategoriaPlatillo {
        @SerializedName("id")
        private int Id;

        @SerializedName("nombre")
        private String Nombre;

        @SerializedName("imagen")
        private String Imagen;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }

        public String getImagen() {
            return Imagen;
        }

        public void setImagen(String imagen) {
            Imagen = imagen;
        }

}
