package com.example.menuint.Modelos;

import com.google.gson.annotations.SerializedName;

public class ModeloIngrediente {
        @SerializedName("id")
        private int Id;

        @SerializedName("nombre")
        private String Nombre;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            Nombre = nombre;
        }
}
